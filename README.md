# BindingLibrariesiOS
Hướng dẫn 
Step gắn LINE SDK for iOS Swift (library dành cho Native) vào project Xamarin.iOS bằng cách dùng BindingLibrary như sau.    
※ Ghi các step này để nhắc nhở nhưng khi đã Push vào repository một lần thì dev chỉ cần Pull là có thể build được nên không cần thực hiện các step bên dưới.

Mục tiền đề    
Để thực hiện các step này cần install software sau.    
・https://cocoapods.org/    
・https://docs.microsoft.com/en-us/xamarin/cross-platform/macios/binding/objective-sharpie/get-started?context=xamarin/mac

Step
1.Download source code    
Chạy command sau, download sourcecode vào chỗ tùy ý.

$ sharpie pod init iphoneos12.1 LineSDKSwift/ObjC
※Về parameter    
　・iphoneos12.1    
　　　Phụ thuộc vào version của xcode. Hãy check bằng command sau.
　　　
　　　$ sharpie xcode -sdks    
　　　sdk: appletvos12.1    arch: arm64       
　　　sdk: iphoneos12.1     arch: arm64   armv7       
　　　sdk: macosx10.14      arch: x86_64  i386        
　　　sdk: watchos5.1       arch: armv7k  
Các step bên dưới toàn bộ đều ghi dựa trên máy iphoneos12.1 nên hãy thay thế sao cho phù hợp.    
・LineSDKSwift/ObjC    
Đây là project của source cần download.    
Hiện tại BindingLibrary chưa thể support cho library được tạo bằng class Swift.    
Do đó, sẽ sử dụng LineSDKSwift/ObjC đã được wrap dành riêng cho Objective-C.

【Chú ý】Với những path như file input vào command trong các step bên dưới, sẽ mô tả bằng relative path lấy từ path đã thực thi command bên trên.

2.Edit, install Podfile    
    Do có file Podfile trong sourcecode đã download ở 1 nên hãy edit theo các step sau

    platform :ios, '10.0'    
    install! 'cocoapods', :integrate_targets => false    
    target 'ObjectiveSharpieIntegration' do    
    use_frameworks!    
    end    
    pod 'LineSDKSwift/ObjC', '~> 5.0'    
    Hãy chạy command sau:

    $ pod install
3.Edit project bằng xcode    
    Mở Pods/Pods.xcodeproj, edit như sau từ BuildSettings    
    ・Setting Enable Bitcode thành No    
    ・Setting vào version cần có Deployment Target (Lần này set vào 10)

4.Build library    
    Hãy chạy command sau.

    $ sharpie pod bind    
5.Build dành riêng cho simulator 
    Ở trạng thái 4 simulator không thể action nên hãy chạy command sau và build dành riêng cho simulator.

    $ xcodebuild -project Pods/Pods.xcodeproj -target LineSDK -sdk iphonesimulator12.1 -configuration Release build
    ※ iphonesimulator12.1 cũng phụ thuộc vào version xcode đang install.

6.Combine library    
    Combine binary đã build ở bước 4 và 5 bằng lipo command    
    Hãy chạy command sau.

    $ lipo -create Binding/LineSDK.framework/LineSDK build/Release-iphonesimulator/LineSDKSwift/LineSDK.framework/LineSDK -output LineSDK    
    Có thể check kết quả combine bằng command sau.

    $ lipo -info LineSDK    
    Architectures in the fat file: LineSDK are: i386 x86_64 armv7 arm64
    Tiếp theo replace binary đã combine thành output của Objective Sharpie.    
    Hãy chạy command sau.

    $ mv LineSDK Binding/LineSDK.framework/LineSDK    
7.Copy file SwiftModule dùng cho simulator.
    Do cần SwiftModule i386 và x86_64 nên sẽ copy vào output Objective Sharpie 
    Hãy chạy command sau.

    $ cp build/Release-iphonesimulator/LineSDKSwift/LineSDK.framework/Modules/LineSDK.swiftmodule/* Binding/LineSDK.framework/Modules/LineSDK.swiftmodule/
8.Tạo ApiDefinition cơ sở    
    Hãy chạy command sau.

    $ sharpie bind -output Output -namespace LineSDK -sdk iphoneos12.1 -scope Binding/LineSDK.framework/Headers Binding/LineSDK.framework/Headers/*.h
    File ApiDefinitions.cs、StructsAndEnums.cs được tạo trong Output directory.

9.Tạo project BindingLibrary
    Tạo project BindingLibrary theo thao tác sau.    
    Click phải vào Solution> Add project mới    
    iOS>library>binding library

10.Add LineSDK.framework vào project
    Copy LineSDK.framework（Binding/LineSDK.framework）đã tạo vào ngay bên dưới porject đã tạo ở 9,    
    Click phải vào Native reference của project BindingLibrary>chỉ định LineSDK.framework ghi trên từ phần add Native reference.

11.Add ApiDefinition    
    Add ApiDefinitions.cs、StructsAndEnums.cs đã tạo ở 8 vào project BidingLibrary.    
    Click phải vào các file >chọn build action, check vào từng ObjcBindingApiDefenition、ObjcBidingCoreSource.    
    Hãy xóa ApiDefinition.cs、StructsAndEnum.cs đã generate tự động khi tạo project BidingLibrary

12.Edit ApiDefinition    
    Edit ApiDefinitions.cs、StructsAndEnums.cs, giải quyết error khi thực thi hay error build    
    Nội dung edit như sau.

    ApiDefinitions.cs    
    ・Xử lý tên class Swift    
    Class Swift sẽ bị thay đổi tên ngay cả khi trong native library nên cần phải thêm chỉ định alias vào BaseTypeAttribute.    
    Tham chiếu LineSDK.framework/Headers/LineSDK-Swift.h, chỉ định tên class được ghi bằng SWIFT_CLASS macro trong BaseTypeAttribute.    
    Ví dụ như sau:

    LineSDK-Swift.h

    SWIFT_CLASS("_TtC7LineSDK10LineSDKAPI")    
    ApiDefinitions.cs (Before)

    // @interface LineSDKAPI : NSObject
    [BaseType (typeof(NSObject))]
    interface LineSDKAPI
    ApiDefinitions.cs (After)

    // @interface LineSDKAPI : NSObject
    [BaseType (typeof(NSObject), Name = "_TtC7LineSDK10LineSDKAPI")]
    interface LineSDKAPI
    ・Xóa class Constants    
    Do class Constants sẽ trở thành error build nên hãy xóa toàn bộ mô tả.    
    ・Thay đổi class của NSSet<LineSDKPermission>    
    Định nghĩa của NSSet<LineSDKPermission> sẽ trở thành error build nên hãy thay đổi toàn bộ định nghĩa thành NSSet<NSObject>

    ・Thay đổi thuộc tính interface của LineSDKLoginButtonDelegate    
    Do sẽ trở thành error build nên hãy sửa như sau.    
    Before

    // @protocol LineSDKLoginButtonDelegate
    [Protocol, Model]
    interface LineSDKLoginButtonDelegate
    After

    // @protocol LineSDKLoginButtonDelegate    
    [BaseType(typeof(NSObject), Name = "_TtP7LineSDK26LineSDKLoginButtonDelegate_")] // Name = "~"はSwiftクラスの名前解決対応    
    [Model]    
    interface LineSDKLoginButtonDelegate
    StructsAndEnums.cs    
    ・Thay đổi type uint thành type ulong    
    Do sẽ trở thành error build nên hãy thay đổi toàn bộ định nghĩa của type uint sang type ulong.

    Về kết quả edit, tham khảo chi tiết bên dưới.    
    https://gs2011.backlog.jp/git/LINE/LineTest/blob/master/LineSDK.Swift/ApiDefinitions.cs    
    https://gs2011.backlog.jp/git/LINE/LineTest/blob/master/LineSDK.Swift/StructsAndEnums.cs

13.Add file linkwith
    Hãy add file sau vào project BindingLibrary.    
    LineSDK.framework.linkwith.cs

    using ObjCRuntime;    

    [assembly: LinkWith("LineSDK.framework",    
    LinkTarget.Arm64 | LinkTarget.ArmV7 | LinkTarget.ArmV7s | LinkTarget.Simulator | LinkTarget.Simulator64,    
    SmartLink = true,    
    ForceLoad = true,    
    LinkerFlags = "-lxml2 -ObjC",    
    Frameworks = "CoreFoundation CoreGraphics CoreImage Foundation Metal QuartzCore UIKit")]    
    Sau khi thêm hãy click phải>chọn build action và check vào "Compile".

    Đến đây việc tạo project BindingLibrary đã hoàn tất.

14.Add reference vào project Xamarin.iOS
    Hãy add thêm reference vào project BindingLibrary đã tạo từ định nghĩa reference của project Xamarin.iOS đối tượng.

15.Giải quyết vấn đề thiếu Swift dependency
    Trong bản build Xamarin.iOS đang thiếu vài chức năng của Swift nên khi chạy app ở trạng thái bước 14 app sẽ bị crash.    
    Để giải quyết vấn đề này sẽ install package Nuget sau vào project Xamarin.iOS    
    ・Xamarin.Swift4    
    ・Xamarin.Swift4.Core    
    ・Xamarin.Swift4.CoreFoundation    
    ・Xamarin.Swift4.CoreGraphics    
    ・Xamarin.Swift4.CoreImage    
    ・Xamarin.Swift4.Darwin    
    ・Xamarin.Swift4.Dispatch    
    ・Xamarin.Swift4.Foundation    
    ・Xamarin.Swift4.Metal    
    ・Xamarin.Swift4.ObjectiveC    
    ・Xamarin.Swift4.OS    
    ・Xamarin.Swift4.QuartzCore    
    ・Xamarin.Swift4.UIKit    
    ※Lần này sẽ dùng xcode10.1, do version Swift là 4 nên đang install Xamarin.Swift4~    
    　Về Nuget sẽ install thực tế, hãy check Swift version tương thích với xcode    
    　　Nói tóm lại, Nuget package ghi trên không phải microsoft mà là library của bên thứ ba nên    
　　　hình như không có Nuget library support cho Swift5 ở thời điểm 2019/04/01
　　　
　　　Bên trên là step gắn Xamarin của LINE SDK for iOS Swift
　　　
　　　URL tham khảo    
　　　https://developers.line.biz/ja/docs/ios-sdk/swift/using-objc/    
　　　https://medium.com/@Flash3001/binding-swift-libraries-xamarin-ios-ff32adbc7c76    
　　　https://qiita.com/iseebi/items/70e70c2d881c5ce27f4e

