#pragma clang diagnostic ignored "-Wdeprecated-declarations"
#pragma clang diagnostic ignored "-Wtypedef-redefinition"
#pragma clang diagnostic ignored "-Wobjc-designated-initializers"
#define DEBUG 1
#include <stdarg.h>
#include <objc/objc.h>
#include <objc/runtime.h>
#include <objc/message.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>

@class UIApplicationDelegate;
@class UIKit_UIControlEventProxy;
@class Foundation_NSDispatcher;
@class __MonoMac_NSSynchronizationContextDispatcher;
@class Foundation_NSAsyncDispatcher;
@class __MonoMac_NSAsyncSynchronizationContextDispatcher;
@class AppDelegate;
@class ViewController;
@class UIKit_UIView_UIViewAppearance;
@class UIKit_UIControl_UIControlAppearance;
@class __NSObject_Disposer;
@protocol LOTAnimatedControl;
@protocol LOTAnimatedSwitch;
@protocol LOTAnimationCache;
@protocol LOTAnimationTransitionController;
@protocol LOTAnimationView;
@protocol LOTCacheProvider;
@protocol LOTValueDelegate;
@protocol LOTColorValueDelegate;
@protocol LOTColorBlockCallback;
@protocol LOTColorValueCallback;
@class LOTValueDelegate;
@class LOTColorValueDelegate;
@protocol LOTComposition;
@protocol LOTNumberValueDelegate;
@protocol LOTFloatInterpolatorCallback;
@protocol LOTImageCache;
@class LOTImageCache;
@protocol LOTKeypath;
@protocol LOTNumberBlockCallback;
@protocol LOTNumberValueCallback;
@class LOTNumberValueDelegate;
@protocol LOTPathValueDelegate;
@protocol LOTPathBlockCallback;
@protocol LOTPathValueCallback;
@class LOTPathValueDelegate;
@protocol LOTPointValueDelegate;
@protocol LOTPointBlockCallback;
@protocol LOTPointInterpolatorCallback;
@protocol LOTPointValueCallback;
@class LOTPointValueDelegate;
@protocol LOTSizeValueDelegate;
@protocol LOTSizeBlockCallback;
@protocol LOTSizeInterpolatorCallback;
@protocol LOTSizeValueCallback;
@class LOTSizeValueDelegate;
@class Lottie_LOTAnimatedControl_LOTAnimatedControlAppearance;
@class Lottie_LOTAnimatedSwitch_LOTAnimatedSwitchAppearance;
@class Lottie_LOTAnimationView_LOTAnimationViewAppearance;

@interface UIApplicationDelegate : NSObject<UIApplicationDelegate> {
}
	-(id) init;
@end

@interface AppDelegate : NSObject<UIApplicationDelegate, UIApplicationDelegate> {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(UIWindow *) window;
	-(void) setWindow:(UIWindow *)p0;
	-(BOOL) application:(UIApplication *)p0 didFinishLaunchingWithOptions:(NSDictionary *)p1;
	-(void) applicationWillResignActive:(UIApplication *)p0;
	-(void) applicationDidEnterBackground:(UIApplication *)p0;
	-(void) applicationWillEnterForeground:(UIApplication *)p0;
	-(void) applicationDidBecomeActive:(UIApplication *)p0;
	-(void) applicationWillTerminate:(UIApplication *)p0;
	-(BOOL) conformsToProtocol:(void *)p0;
	-(id) init;
@end

@interface ViewController : UIViewController {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(void) viewDidLoad;
	-(void) didReceiveMemoryWarning;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface UIKit_UIView_UIViewAppearance : NSObject {
}
	-(void) release;
	-(id) retain;
	-(int) xamarinGetGCHandle;
	-(void) xamarinSetGCHandle: (int) gchandle;
	-(BOOL) conformsToProtocol:(void *)p0;
@end

@interface UIKit_UIControl_UIControlAppearance : UIKit_UIView_UIViewAppearance {
}
@end

@protocol LOTAnimatedControl
	@optional @property (nonatomic, assign, readonly) id animationView;
	@optional @property (nonatomic, assign) id animationComp;
	@optional -(void) setLayerName:(NSString *)p0 forState:(NSUInteger)p1;
@end

@protocol LOTAnimatedSwitch
	@optional @property (nonatomic, assign, getter = isOn) BOOL on;
	@optional @property (nonatomic, assign) BOOL interactiveGesture;
	@optional +(id) switchNamed:(NSString *)p0;
	@optional +(id) switchNamed:(NSString *)p0 inBundle:(NSBundle *)p1;
	@optional -(void) setOn:(BOOL)p0 animated:(BOOL)p1;
	@optional -(void) setProgressRangeForOnState:(CGFloat)p0 toProgress:(CGFloat)p1;
	@optional -(void) setProgressRangeForOffState:(CGFloat)p0 toProgress:(CGFloat)p1;
@end

@protocol LOTAnimationCache
	@optional +(id) sharedCache;
	@optional -(void) addAnimation:(id)p0 forKey:(NSString *)p1;
	@optional -(id) animationForKey:(NSString *)p0;
	@optional -(void) removeAnimationForKey:(NSString *)p0;
	@optional -(void) clearCache;
	@optional -(void) disableCaching;
@end

@protocol LOTAnimationTransitionController
@end

@protocol LOTAnimationView
	@optional @property (nonatomic, assign, readonly) BOOL isAnimationPlaying;
	@optional @property (nonatomic, assign) BOOL loopAnimation;
	@optional @property (nonatomic, assign) BOOL autoReverseAnimation;
	@optional @property (nonatomic, assign) CGFloat animationProgress;
	@optional @property (nonatomic, assign) CGFloat animationSpeed;
	@optional @property (nonatomic, assign, readonly) CGFloat animationDuration;
	@optional @property (nonatomic, assign) BOOL cacheEnable;
	@optional @property (nonatomic, assign) BOOL shouldRasterizeWhenIdle;
	@optional @property (nonatomic, copy) id completionBlock;
	@optional @property (nonatomic, retain) id sceneModel;
	@optional +(id) animationNamed:(NSString *)p0;
	@optional +(id) animationNamed:(NSString *)p0 inBundle:(NSBundle *)p1;
	@optional +(id) animationFromJSON:(NSDictionary *)p0;
	@optional +(id) animationWithFilePath:(NSString *)p0;
	@optional +(id) animationFromJSON:(NSDictionary *)p0 inBundle:(NSBundle *)p1;
	@optional -(void) setAnimationNamed:(NSString *)p0;
	@optional -(void) playToProgress:(CGFloat)p0 withCompletion:(id)p1;
	@optional -(void) playFromProgress:(CGFloat)p0 toProgress:(CGFloat)p1 withCompletion:(id)p2;
	@optional -(void) playToFrame:(NSNumber *)p0 withCompletion:(id)p1;
	@optional -(void) playFromFrame:(NSNumber *)p0 toFrame:(NSNumber *)p1 withCompletion:(id)p2;
	@optional -(void) playWithCompletion:(id)p0;
	@optional -(void) play;
	@optional -(void) pause;
	@optional -(void) stop;
	@optional -(void) setProgressWithFrame:(NSNumber *)p0;
	@optional -(void) forceDrawingUpdate;
	@optional -(void) logHierarchyKeypaths;
	@optional -(void) setValueDelegate:(NSObject *)p0 forKeypath:(id)p1;
	@optional -(NSArray *) keysForKeyPath:(id)p0;
	@optional -(CGPoint) convertPoint:(CGPoint)p0 toKeypathLayer:(id)p1;
	@optional -(CGRect) convertRect:(CGRect)p0 toKeypathLayer:(id)p1;
	@optional -(CGPoint) convertPoint:(CGPoint)p0 fromKeypathLayer:(id)p1;
	@optional -(CGRect) convertRect:(CGRect)p0 fromKeypathLayer:(id)p1;
	@optional -(void) addSubview:(UIView *)p0 toKeypathLayer:(id)p1;
	@optional -(void) maskSubview:(UIView *)p0 toKeypathLayer:(id)p1;
	@optional -(void) setValue:(NSObject *)p0 forKeypath:(NSString *)p1 atFrame:(NSNumber *)p2;
	@optional -(void) addSubview:(UIView *)p0 toLayerNamed:(NSString *)p1 applyTransform:(BOOL)p2;
	@optional -(CGRect) convertRect:(CGRect)p0 toLayerNamed:(NSString *)p1;
@end

@protocol LOTCacheProvider
	@optional +(id) imageCache;
	@optional +(void) setImageCache:(id)p0;
@end

@protocol LOTValueDelegate
@end

@protocol LOTColorValueDelegate
	@required -(id) colorForFrame:(CGFloat)p0 startKeyframe:(CGFloat)p1 endKeyframe:(CGFloat)p2 interpolatedProgress:(CGFloat)p3 startColor:(id)p4 endColor:(id)p5 currentColor:(id)p6;
@end

@protocol LOTColorBlockCallback
	@optional @property (nonatomic, copy) id callback;
	@optional +(id) withBlock:(id)p0;
@end

@protocol LOTColorValueCallback
	@optional @property (nonatomic, assign) id colorValue;
	@optional +(id) withCGColor:(id)p0;
@end

@interface LOTValueDelegate : NSObject<LOTValueDelegate> {
}
	-(id) init;
@end

@interface LOTColorValueDelegate : NSObject<LOTColorValueDelegate, LOTValueDelegate> {
}
	-(id) init;
@end

@protocol LOTComposition
	@optional @property (nonatomic, assign, readonly) CGRect compBounds;
	@optional @property (nonatomic, assign, readonly) NSNumber * startFrame;
	@optional @property (nonatomic, assign, readonly) NSNumber * endFrame;
	@optional @property (nonatomic, assign, readonly) NSNumber * framerate;
	@optional @property (nonatomic, assign, readonly) double timeDuration;
	@optional @property (nonatomic, assign) NSString * rootDirectory;
	@optional @property (nonatomic, assign, readonly) NSBundle * assetBundle;
	@optional @property (nonatomic, assign) NSString * cacheKey;
	@optional +(id) animationNamed:(NSString *)p0;
	@optional +(id) animationNamed:(NSString *)p0 inBundle:(NSBundle *)p1;
	@optional +(id) animationWithFilePath:(NSString *)p0;
	@optional +(id) animationFromJSON:(NSDictionary *)p0;
	@optional +(id) animationFromJSON:(NSDictionary *)p0 inBundle:(NSBundle *)p1;
@end

@protocol LOTNumberValueDelegate
	@required -(CGFloat) floatValueForFrame:(CGFloat)p0 startKeyframe:(CGFloat)p1 endKeyframe:(CGFloat)p2 interpolatedProgress:(CGFloat)p3 startValue:(CGFloat)p4 endValue:(CGFloat)p5 currentValue:(CGFloat)p6;
@end

@protocol LOTFloatInterpolatorCallback
	@optional @property (nonatomic, assign) CGFloat fromFloat;
	@optional @property (nonatomic, assign) CGFloat toFloat;
	@optional @property (nonatomic, assign) CGFloat currentProgress;
	@optional +(id) withFromFloat:(CGFloat)p0 toFloat:(CGFloat)p1;
@end

@protocol LOTImageCache
	@required -(UIImage *) imageForKey:(NSString *)p0;
	@required -(void) setImage:(UIImage *)p0 forKey:(NSString *)p1;
@end

@interface LOTImageCache : NSObject<LOTImageCache> {
}
	-(id) init;
@end

@protocol LOTKeypath
	@optional @property (nonatomic, assign, readonly) NSString * absoluteKeypath;
	@optional @property (nonatomic, assign, readonly) NSString * currentKey;
	@optional @property (nonatomic, assign, readonly) NSString * currentKeyPath;
	@optional @property (nonatomic, assign, readonly) NSDictionary * searchResults;
	@optional @property (nonatomic, assign, readonly) BOOL hasFuzzyWildcard;
	@optional @property (nonatomic, assign, readonly) BOOL hasWildcard;
	@optional @property (nonatomic, assign, readonly) BOOL endOfKeypath;
	@optional +(id) keypathWithString:(NSString *)p0;
	@optional +(id) keypathWithKeys:(NSString *)p0, ...;
	@optional -(BOOL) pushKey:(NSString *)p0;
	@optional -(void) popKey;
	@optional -(void) popToRootKey;
	@optional -(void) addSearchResultForCurrentPath:(NSObject *)p0;
@end

@protocol LOTNumberBlockCallback
	@optional @property (nonatomic, copy) id callback;
	@optional +(id) withBlock:(id)p0;
@end

@protocol LOTNumberValueCallback
	@optional @property (nonatomic, assign) CGFloat numberValue;
	@optional +(id) withFloatValue:(CGFloat)p0;
@end

@interface LOTNumberValueDelegate : NSObject<LOTNumberValueDelegate, LOTValueDelegate> {
}
	-(id) init;
@end

@protocol LOTPathValueDelegate
	@required -(id) pathForFrame:(CGFloat)p0 startKeyframe:(CGFloat)p1 endKeyframe:(CGFloat)p2 interpolatedProgress:(CGFloat)p3;
@end

@protocol LOTPathBlockCallback
	@optional @property (nonatomic, copy) id callback;
	@optional +(id) withBlock:(id)p0;
@end

@protocol LOTPathValueCallback
	@optional @property (nonatomic, assign) id pathValue;
	@optional +(id) withCGPath:(id)p0;
@end

@interface LOTPathValueDelegate : NSObject<LOTPathValueDelegate, LOTValueDelegate> {
}
	-(id) init;
@end

@protocol LOTPointValueDelegate
	@required -(CGPoint) pointForFrame:(CGFloat)p0 startKeyframe:(CGFloat)p1 endKeyframe:(CGFloat)p2 interpolatedProgress:(CGFloat)p3 startPoint:(CGPoint)p4 endPoint:(CGPoint)p5 currentPoint:(CGPoint)p6;
@end

@protocol LOTPointBlockCallback
	@optional @property (nonatomic, copy) id callback;
	@optional +(id) withBlock:(id)p0;
@end

@protocol LOTPointInterpolatorCallback
	@optional @property (nonatomic, assign) CGPoint fromPoint;
	@optional @property (nonatomic, assign) CGPoint toPoint;
	@optional @property (nonatomic, assign) CGFloat currentProgress;
	@optional +(id) withFromPoint:(CGPoint)p0 toPoint:(CGPoint)p1;
@end

@protocol LOTPointValueCallback
	@optional @property (nonatomic, assign) CGPoint pointValue;
	@optional +(id) withPointValue:(CGPoint)p0;
@end

@interface LOTPointValueDelegate : NSObject<LOTPointValueDelegate, LOTValueDelegate> {
}
	-(id) init;
@end

@protocol LOTSizeValueDelegate
	@required -(CGSize) sizeForFrame:(CGFloat)p0 startKeyframe:(CGFloat)p1 endKeyframe:(CGFloat)p2 interpolatedProgress:(CGFloat)p3 startSize:(CGSize)p4 endSize:(CGSize)p5 currentSize:(CGSize)p6;
@end

@protocol LOTSizeBlockCallback
	@optional @property (nonatomic, copy) id callback;
	@optional +(id) withBlock:(id)p0;
@end

@protocol LOTSizeInterpolatorCallback
	@optional @property (nonatomic, assign) CGSize fromSize;
	@optional @property (nonatomic, assign) CGSize toSize;
	@optional @property (nonatomic, assign) CGFloat currentProgress;
	@optional +(id) withFromSize:(CGSize)p0 toSize:(CGSize)p1;
@end

@protocol LOTSizeValueCallback
	@optional @property (nonatomic, assign) CGSize sizeValue;
	@optional +(id) withPointValue:(CGSize)p0;
@end

@interface LOTSizeValueDelegate : NSObject<LOTSizeValueDelegate, LOTValueDelegate> {
}
	-(id) init;
@end

@interface Lottie_LOTAnimatedControl_LOTAnimatedControlAppearance : UIKit_UIControl_UIControlAppearance {
}
@end

@interface Lottie_LOTAnimatedSwitch_LOTAnimatedSwitchAppearance : Lottie_LOTAnimatedControl_LOTAnimatedControlAppearance {
}
@end

@interface Lottie_LOTAnimationView_LOTAnimationViewAppearance : UIKit_UIView_UIViewAppearance {
}
@end


