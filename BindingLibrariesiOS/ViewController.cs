﻿using System;
using System.Diagnostics;
using UIKit;
using Lottie;
using Foundation;

namespace BindingLibrariesiOS
{
    public partial class ViewController : UIViewController
    {
        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            var bunld = new NSBundle().BundlePath;
            //LOTAnimationView animationView = LOTAnimationView.AnimationNamed("LottieLogo1");
            //if (animationView != null)
            //{
            //    this.View.AddSubview(animationView);
            //    animationView.PlayWithCompletion((animationFinished) =>
            //    {
            //        Debug.WriteLine("animationFinished");
            //    });
            //}   splash

            var bundle = NSBundle.MainBundle;

            var a = new CompatibleAnimation("CheckMarkSuccessData", bundle);

            var viewAnimation = new CompatibleAnimationView
            {
                Frame = new CoreGraphics.CGRect(0, 0, 250, 500),
                CompatibleAnimation = a
            };

            viewAnimation.Play();
            viewAnimation.LoopAnimationCount = nfloat.MaxValue;

            this.View.AddSubview(viewAnimation);



        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}
