//
// Auto-generated from generator.cs, do not edit
//
// We keep references to objects, so warning 414 is expected

#pragma warning disable 414

using System;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using UIKit;
using GLKit;
using Metal;
using CoreML;
using MapKit;
using Photos;
using ModelIO;
using SceneKit;
using Contacts;
using Security;
using Messages;
using AudioUnit;
using CoreVideo;
using CoreMedia;
using QuickLook;
using CoreImage;
using SpriteKit;
using Foundation;
using CoreMotion;
using ObjCRuntime;
using AddressBook;
using MediaPlayer;
using GameplayKit;
using CoreGraphics;
using CoreLocation;
using AVFoundation;
using NewsstandKit;
using FileProvider;
using CoreAnimation;
using CoreFoundation;

namespace Lottie {
	[Register("_TtC6Lottie23CompatibleAnimationView", true)]
	public unsafe partial class CompatibleAnimationView : global::UIKit.UIView {
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		static readonly IntPtr class_ptr = Class.GetHandle ("_TtC6Lottie23CompatibleAnimationView");
		
		public override IntPtr ClassHandle { get { return class_ptr; } }
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		[Export ("init")]
		public CompatibleAnimationView () : base (NSObjectFlag.Empty)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
			if (IsDirectBinding) {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend (this.Handle, global::ObjCRuntime.Selector.GetHandle ("init")), "init");
			} else {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSendSuper (this.SuperHandle, global::ObjCRuntime.Selector.GetHandle ("init")), "init");
			}
		}

		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[DesignatedInitializer]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		[Export ("initWithCoder:")]
		public CompatibleAnimationView (NSCoder coder) : base (NSObjectFlag.Empty)
		{

			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
			if (IsDirectBinding) {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("initWithCoder:"), coder.Handle), "initWithCoder:");
			} else {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("initWithCoder:"), coder.Handle), "initWithCoder:");
			}
		}

		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		protected CompatibleAnimationView (NSObjectFlag t) : base (t)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
		}

		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		protected internal CompatibleAnimationView (IntPtr handle) : base (handle)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
		}

		[Export ("initWithFrame:")]
		[DesignatedInitializer]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public CompatibleAnimationView (CGRect frame)
			: base (NSObjectFlag.Empty)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
			if (IsDirectBinding) {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend_CGRect (this.Handle, Selector.GetHandle ("initWithFrame:"), frame), "initWithFrame:");
			} else {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSendSuper_CGRect (this.SuperHandle, Selector.GetHandle ("initWithFrame:"), frame), "initWithFrame:");
			}
		}
		
		[Export ("addSubview:forLayerAt:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual void AddSubview (AnimationSubview subview, CompatibleAnimationKeypath keypath)
		{
			if (subview == null)
				throw new ArgumentNullException ("subview");
			if (keypath == null)
				throw new ArgumentNullException ("keypath");
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend_IntPtr_IntPtr (this.Handle, Selector.GetHandle ("addSubview:forLayerAt:"), subview.Handle, keypath.Handle);
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper_IntPtr_IntPtr (this.SuperHandle, Selector.GetHandle ("addSubview:forLayerAt:"), subview.Handle, keypath.Handle);
			}
		}
		
		[Export ("convertWithPoint:toLayerAt:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual CGPoint ConvertWithPoint (CGPoint point, CompatibleAnimationKeypath keypath)
		{
			CGPoint ret;
			if (IsDirectBinding) {
				if (Runtime.Arch == Arch.DEVICE) {
					if (IntPtr.Size == 8) {
						ret = global::ApiDefinitions.Messaging.CGPoint_objc_msgSend_CGPoint_IntPtr (this.Handle, Selector.GetHandle ("convertWithPoint:toLayerAt:"), point, keypath == null ? IntPtr.Zero : keypath.Handle);
					} else {
						global::ApiDefinitions.Messaging.CGPoint_objc_msgSend_stret_CGPoint_IntPtr (out ret, this.Handle, Selector.GetHandle ("convertWithPoint:toLayerAt:"), point, keypath == null ? IntPtr.Zero : keypath.Handle);
					}
				} else if (IntPtr.Size == 8) {
					ret = global::ApiDefinitions.Messaging.CGPoint_objc_msgSend_CGPoint_IntPtr (this.Handle, Selector.GetHandle ("convertWithPoint:toLayerAt:"), point, keypath == null ? IntPtr.Zero : keypath.Handle);
				} else {
					ret = global::ApiDefinitions.Messaging.CGPoint_objc_msgSend_CGPoint_IntPtr (this.Handle, Selector.GetHandle ("convertWithPoint:toLayerAt:"), point, keypath == null ? IntPtr.Zero : keypath.Handle);
				}
			} else {
				if (Runtime.Arch == Arch.DEVICE) {
					if (IntPtr.Size == 8) {
						ret = global::ApiDefinitions.Messaging.CGPoint_objc_msgSendSuper_CGPoint_IntPtr (this.SuperHandle, Selector.GetHandle ("convertWithPoint:toLayerAt:"), point, keypath == null ? IntPtr.Zero : keypath.Handle);
					} else {
						global::ApiDefinitions.Messaging.CGPoint_objc_msgSendSuper_stret_CGPoint_IntPtr (out ret, this.SuperHandle, Selector.GetHandle ("convertWithPoint:toLayerAt:"), point, keypath == null ? IntPtr.Zero : keypath.Handle);
					}
				} else if (IntPtr.Size == 8) {
					ret = global::ApiDefinitions.Messaging.CGPoint_objc_msgSendSuper_CGPoint_IntPtr (this.SuperHandle, Selector.GetHandle ("convertWithPoint:toLayerAt:"), point, keypath == null ? IntPtr.Zero : keypath.Handle);
				} else {
					ret = global::ApiDefinitions.Messaging.CGPoint_objc_msgSendSuper_CGPoint_IntPtr (this.SuperHandle, Selector.GetHandle ("convertWithPoint:toLayerAt:"), point, keypath == null ? IntPtr.Zero : keypath.Handle);
				}
			}
			return ret;
		}
		
		[Export ("convertWithRect:toLayerAt:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual CGRect ConvertWithRect (CGRect rect, CompatibleAnimationKeypath keypath)
		{
			CGRect ret;
			if (IsDirectBinding) {
				if (Runtime.Arch == Arch.DEVICE) {
					if (IntPtr.Size == 8) {
						ret = global::ApiDefinitions.Messaging.CGRect_objc_msgSend_CGRect_IntPtr (this.Handle, Selector.GetHandle ("convertWithRect:toLayerAt:"), rect, keypath == null ? IntPtr.Zero : keypath.Handle);
					} else {
						global::ApiDefinitions.Messaging.CGRect_objc_msgSend_stret_CGRect_IntPtr (out ret, this.Handle, Selector.GetHandle ("convertWithRect:toLayerAt:"), rect, keypath == null ? IntPtr.Zero : keypath.Handle);
					}
				} else if (IntPtr.Size == 8) {
					global::ApiDefinitions.Messaging.CGRect_objc_msgSend_stret_CGRect_IntPtr (out ret, this.Handle, Selector.GetHandle ("convertWithRect:toLayerAt:"), rect, keypath == null ? IntPtr.Zero : keypath.Handle);
				} else {
					global::ApiDefinitions.Messaging.CGRect_objc_msgSend_stret_CGRect_IntPtr (out ret, this.Handle, Selector.GetHandle ("convertWithRect:toLayerAt:"), rect, keypath == null ? IntPtr.Zero : keypath.Handle);
				}
			} else {
				if (Runtime.Arch == Arch.DEVICE) {
					if (IntPtr.Size == 8) {
						ret = global::ApiDefinitions.Messaging.CGRect_objc_msgSendSuper_CGRect_IntPtr (this.SuperHandle, Selector.GetHandle ("convertWithRect:toLayerAt:"), rect, keypath == null ? IntPtr.Zero : keypath.Handle);
					} else {
						global::ApiDefinitions.Messaging.CGRect_objc_msgSendSuper_stret_CGRect_IntPtr (out ret, this.SuperHandle, Selector.GetHandle ("convertWithRect:toLayerAt:"), rect, keypath == null ? IntPtr.Zero : keypath.Handle);
					}
				} else if (IntPtr.Size == 8) {
					global::ApiDefinitions.Messaging.CGRect_objc_msgSendSuper_stret_CGRect_IntPtr (out ret, this.SuperHandle, Selector.GetHandle ("convertWithRect:toLayerAt:"), rect, keypath == null ? IntPtr.Zero : keypath.Handle);
				} else {
					global::ApiDefinitions.Messaging.CGRect_objc_msgSendSuper_stret_CGRect_IntPtr (out ret, this.SuperHandle, Selector.GetHandle ("convertWithRect:toLayerAt:"), rect, keypath == null ? IntPtr.Zero : keypath.Handle);
				}
			}
			return ret;
		}
		
		[Export ("forceDisplayUpdate")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual void ForceDisplayUpdate ()
		{
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend (this.Handle, Selector.GetHandle ("forceDisplayUpdate"));
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("forceDisplayUpdate"));
			}
		}
		
		[Export ("frameTimeForMarker:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual nfloat FrameTimeForMarker (string named)
		{
			if (named == null)
				throw new ArgumentNullException ("named");
			var nsnamed = NSString.CreateNative (named);
			
			nfloat ret;
			if (IsDirectBinding) {
				ret = global::ApiDefinitions.Messaging.nfloat_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("frameTimeForMarker:"), nsnamed);
			} else {
				ret = global::ApiDefinitions.Messaging.nfloat_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("frameTimeForMarker:"), nsnamed);
			}
			NSString.ReleaseNative (nsnamed);
			
			return ret;
		}
		
		[Export ("getValueFor:atFrame:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual NSObject GetValueFor (CompatibleAnimationKeypath keypath, nfloat atFrame)
		{
			if (keypath == null)
				throw new ArgumentNullException ("keypath");
			if (IsDirectBinding) {
				return Runtime.GetNSObject (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend_IntPtr_nfloat (this.Handle, Selector.GetHandle ("getValueFor:atFrame:"), keypath.Handle, atFrame));
			} else {
				return Runtime.GetNSObject (global::ApiDefinitions.Messaging.IntPtr_objc_msgSendSuper_IntPtr_nfloat (this.SuperHandle, Selector.GetHandle ("getValueFor:atFrame:"), keypath.Handle, atFrame));
			}
		}
		
		[Export ("logHierarchyKeypaths")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual void LogHierarchyKeypaths ()
		{
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend (this.Handle, Selector.GetHandle ("logHierarchyKeypaths"));
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("logHierarchyKeypaths"));
			}
		}
		
		[Export ("pause")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual void Pause ()
		{
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend (this.Handle, Selector.GetHandle ("pause"));
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("pause"));
			}
		}
		
		[Export ("play")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual void Play ()
		{
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend (this.Handle, Selector.GetHandle ("play"));
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("play"));
			}
		}
		
		[Export ("playFromFrame:toFrame:completion:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public unsafe virtual void PlayFromFrame (nfloat fromFrame, nfloat toFrame, [BlockProxy (typeof (ObjCRuntime.Trampolines.NIDActionArity1V0))]global::System.Action<bool> completion)
		{
			BlockLiteral *block_ptr_completion;
			BlockLiteral block_completion;
			if (completion == null){
				block_ptr_completion = null;
			} else {
				block_completion = new BlockLiteral ();
				block_ptr_completion = &block_completion;
				block_completion.SetupBlockUnsafe (Trampolines.SDActionArity1V0.Handler, completion);
			}
			
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend_nfloat_nfloat_IntPtr (this.Handle, Selector.GetHandle ("playFromFrame:toFrame:completion:"), fromFrame, toFrame, (IntPtr) block_ptr_completion);
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper_nfloat_nfloat_IntPtr (this.SuperHandle, Selector.GetHandle ("playFromFrame:toFrame:completion:"), fromFrame, toFrame, (IntPtr) block_ptr_completion);
			}
			if (block_ptr_completion != null)
				block_ptr_completion->CleanupBlock ();
			
		}
		
		[Export ("playFromMarker:toMarker:completion:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public unsafe virtual void PlayFromMarker (string fromMarker, string toMarker, [BlockProxy (typeof (ObjCRuntime.Trampolines.NIDActionArity1V0))]global::System.Action<bool> completion)
		{
			if (fromMarker == null)
				throw new ArgumentNullException ("fromMarker");
			if (toMarker == null)
				throw new ArgumentNullException ("toMarker");
			var nsfromMarker = NSString.CreateNative (fromMarker);
			var nstoMarker = NSString.CreateNative (toMarker);
			BlockLiteral *block_ptr_completion;
			BlockLiteral block_completion;
			if (completion == null){
				block_ptr_completion = null;
			} else {
				block_completion = new BlockLiteral ();
				block_ptr_completion = &block_completion;
				block_completion.SetupBlockUnsafe (Trampolines.SDActionArity1V0.Handler, completion);
			}
			
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend_IntPtr_IntPtr_IntPtr (this.Handle, Selector.GetHandle ("playFromMarker:toMarker:completion:"), nsfromMarker, nstoMarker, (IntPtr) block_ptr_completion);
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper_IntPtr_IntPtr_IntPtr (this.SuperHandle, Selector.GetHandle ("playFromMarker:toMarker:completion:"), nsfromMarker, nstoMarker, (IntPtr) block_ptr_completion);
			}
			NSString.ReleaseNative (nsfromMarker);
			NSString.ReleaseNative (nstoMarker);
			if (block_ptr_completion != null)
				block_ptr_completion->CleanupBlock ();
			
		}
		
		[Export ("playFromProgress:toProgress:completion:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public unsafe virtual void PlayFromProgress (nfloat fromProgress, nfloat toProgress, [BlockProxy (typeof (ObjCRuntime.Trampolines.NIDActionArity1V0))]global::System.Action<bool> completion)
		{
			BlockLiteral *block_ptr_completion;
			BlockLiteral block_completion;
			if (completion == null){
				block_ptr_completion = null;
			} else {
				block_completion = new BlockLiteral ();
				block_ptr_completion = &block_completion;
				block_completion.SetupBlockUnsafe (Trampolines.SDActionArity1V0.Handler, completion);
			}
			
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend_nfloat_nfloat_IntPtr (this.Handle, Selector.GetHandle ("playFromProgress:toProgress:completion:"), fromProgress, toProgress, (IntPtr) block_ptr_completion);
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper_nfloat_nfloat_IntPtr (this.SuperHandle, Selector.GetHandle ("playFromProgress:toProgress:completion:"), fromProgress, toProgress, (IntPtr) block_ptr_completion);
			}
			if (block_ptr_completion != null)
				block_ptr_completion->CleanupBlock ();
			
		}
		
		[Export ("playWithCompletion:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public unsafe virtual void PlayWithCompletion ([BlockProxy (typeof (ObjCRuntime.Trampolines.NIDActionArity1V0))]global::System.Action<bool> completion)
		{
			BlockLiteral *block_ptr_completion;
			BlockLiteral block_completion;
			if (completion == null){
				block_ptr_completion = null;
			} else {
				block_completion = new BlockLiteral ();
				block_ptr_completion = &block_completion;
				block_completion.SetupBlockUnsafe (Trampolines.SDActionArity1V0.Handler, completion);
			}
			
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("playWithCompletion:"), (IntPtr) block_ptr_completion);
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("playWithCompletion:"), (IntPtr) block_ptr_completion);
			}
			if (block_ptr_completion != null)
				block_ptr_completion->CleanupBlock ();
			
		}
		
		[Export ("progressTimeForMarker:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual nfloat ProgressTimeForMarker (string named)
		{
			if (named == null)
				throw new ArgumentNullException ("named");
			var nsnamed = NSString.CreateNative (named);
			
			nfloat ret;
			if (IsDirectBinding) {
				ret = global::ApiDefinitions.Messaging.nfloat_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("progressTimeForMarker:"), nsnamed);
			} else {
				ret = global::ApiDefinitions.Messaging.nfloat_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("progressTimeForMarker:"), nsnamed);
			}
			NSString.ReleaseNative (nsnamed);
			
			return ret;
		}
		
		[Export ("reloadImages")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual void ReloadImages ()
		{
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend (this.Handle, Selector.GetHandle ("reloadImages"));
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("reloadImages"));
			}
		}
		
		[Export ("stop")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual void Stop ()
		{
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend (this.Handle, Selector.GetHandle ("stop"));
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("stop"));
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual nfloat AnimationSpeed {
			[Export ("animationSpeed")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSend (this.Handle, Selector.GetHandle ("animationSpeed"));
				} else {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("animationSpeed"));
				}
			}
			
			[Export ("setAnimationSpeed:")]
			set {
				if (IsDirectBinding) {
					global::ApiDefinitions.Messaging.void_objc_msgSend_nfloat (this.Handle, Selector.GetHandle ("setAnimationSpeed:"), value);
				} else {
					global::ApiDefinitions.Messaging.void_objc_msgSendSuper_nfloat (this.SuperHandle, Selector.GetHandle ("setAnimationSpeed:"), value);
				}
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual CompatibleAnimation CompatibleAnimation {
			[Export ("compatibleAnimation", ArgumentSemantic.Retain)]
			get {
				CompatibleAnimation ret;
				if (IsDirectBinding) {
					ret =  Runtime.GetNSObject<CompatibleAnimation> (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend (this.Handle, Selector.GetHandle ("compatibleAnimation")));
				} else {
					ret =  Runtime.GetNSObject<CompatibleAnimation> (global::ApiDefinitions.Messaging.IntPtr_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("compatibleAnimation")));
				}
				return ret;
			}
			
			[Export ("setCompatibleAnimation:", ArgumentSemantic.Retain)]
			set {
				if (IsDirectBinding) {
					global::ApiDefinitions.Messaging.void_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("setCompatibleAnimation:"), value == null ? IntPtr.Zero : value.Handle);
				} else {
					global::ApiDefinitions.Messaging.void_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("setCompatibleAnimation:"), value == null ? IntPtr.Zero : value.Handle);
				}
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual global::UIKit.UIViewContentMode ContentMode {
			[Export ("contentMode", ArgumentSemantic.UnsafeUnretained)]
			get {
				global::UIKit.UIViewContentMode ret;
				if (IsDirectBinding) {
					if (IntPtr.Size == 8) {
						ret = (global::UIKit.UIViewContentMode) global::ApiDefinitions.Messaging.Int64_objc_msgSend (this.Handle, Selector.GetHandle ("contentMode"));
					} else {
						ret = (global::UIKit.UIViewContentMode) global::ApiDefinitions.Messaging.int_objc_msgSend (this.Handle, Selector.GetHandle ("contentMode"));
					}
				} else {
					if (IntPtr.Size == 8) {
						ret = (global::UIKit.UIViewContentMode) global::ApiDefinitions.Messaging.Int64_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("contentMode"));
					} else {
						ret = (global::UIKit.UIViewContentMode) global::ApiDefinitions.Messaging.int_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("contentMode"));
					}
				}
				return ret;
			}
			
			[Export ("setContentMode:", ArgumentSemantic.UnsafeUnretained)]
			set {
				if (IsDirectBinding) {
					if (IntPtr.Size == 8) {
						global::ApiDefinitions.Messaging.void_objc_msgSend_Int64 (this.Handle, Selector.GetHandle ("setContentMode:"), (Int64)value);
					} else {
						global::ApiDefinitions.Messaging.void_objc_msgSend_int (this.Handle, Selector.GetHandle ("setContentMode:"), (int)value);
					}
				} else {
					if (IntPtr.Size == 8) {
						global::ApiDefinitions.Messaging.void_objc_msgSendSuper_Int64 (this.SuperHandle, Selector.GetHandle ("setContentMode:"), (Int64)value);
					} else {
						global::ApiDefinitions.Messaging.void_objc_msgSendSuper_int (this.SuperHandle, Selector.GetHandle ("setContentMode:"), (int)value);
					}
				}
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual nfloat CurrentFrame {
			[Export ("currentFrame")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSend (this.Handle, Selector.GetHandle ("currentFrame"));
				} else {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("currentFrame"));
				}
			}
			
			[Export ("setCurrentFrame:")]
			set {
				if (IsDirectBinding) {
					global::ApiDefinitions.Messaging.void_objc_msgSend_nfloat (this.Handle, Selector.GetHandle ("setCurrentFrame:"), value);
				} else {
					global::ApiDefinitions.Messaging.void_objc_msgSendSuper_nfloat (this.SuperHandle, Selector.GetHandle ("setCurrentFrame:"), value);
				}
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual nfloat CurrentProgress {
			[Export ("currentProgress")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSend (this.Handle, Selector.GetHandle ("currentProgress"));
				} else {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("currentProgress"));
				}
			}
			
			[Export ("setCurrentProgress:")]
			set {
				if (IsDirectBinding) {
					global::ApiDefinitions.Messaging.void_objc_msgSend_nfloat (this.Handle, Selector.GetHandle ("setCurrentProgress:"), value);
				} else {
					global::ApiDefinitions.Messaging.void_objc_msgSendSuper_nfloat (this.SuperHandle, Selector.GetHandle ("setCurrentProgress:"), value);
				}
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual double CurrentTime {
			[Export ("currentTime")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.Double_objc_msgSend (this.Handle, Selector.GetHandle ("currentTime"));
				} else {
					return global::ApiDefinitions.Messaging.Double_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("currentTime"));
				}
			}
			
			[Export ("setCurrentTime:")]
			set {
				if (IsDirectBinding) {
					global::ApiDefinitions.Messaging.void_objc_msgSend_Double (this.Handle, Selector.GetHandle ("setCurrentTime:"), value);
				} else {
					global::ApiDefinitions.Messaging.void_objc_msgSendSuper_Double (this.SuperHandle, Selector.GetHandle ("setCurrentTime:"), value);
				}
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual nfloat LoopAnimationCount {
			[Export ("loopAnimationCount")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSend (this.Handle, Selector.GetHandle ("loopAnimationCount"));
				} else {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("loopAnimationCount"));
				}
			}
			
			[Export ("setLoopAnimationCount:")]
			set {
				if (IsDirectBinding) {
					global::ApiDefinitions.Messaging.void_objc_msgSend_nfloat (this.Handle, Selector.GetHandle ("setLoopAnimationCount:"), value);
				} else {
					global::ApiDefinitions.Messaging.void_objc_msgSendSuper_nfloat (this.SuperHandle, Selector.GetHandle ("setLoopAnimationCount:"), value);
				}
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual nfloat RealtimeAnimationFrame {
			[Export ("realtimeAnimationFrame")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSend (this.Handle, Selector.GetHandle ("realtimeAnimationFrame"));
				} else {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("realtimeAnimationFrame"));
				}
			}
			
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual nfloat RealtimeAnimationProgress {
			[Export ("realtimeAnimationProgress")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSend (this.Handle, Selector.GetHandle ("realtimeAnimationProgress"));
				} else {
					return global::ApiDefinitions.Messaging.nfloat_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("realtimeAnimationProgress"));
				}
			}
			
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual bool RespectAnimationFrameRate {
			[Export ("respectAnimationFrameRate")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.bool_objc_msgSend (this.Handle, Selector.GetHandle ("respectAnimationFrameRate"));
				} else {
					return global::ApiDefinitions.Messaging.bool_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("respectAnimationFrameRate"));
				}
			}
			
			[Export ("setRespectAnimationFrameRate:")]
			set {
				if (IsDirectBinding) {
					global::ApiDefinitions.Messaging.void_objc_msgSend_bool (this.Handle, Selector.GetHandle ("setRespectAnimationFrameRate:"), value);
				} else {
					global::ApiDefinitions.Messaging.void_objc_msgSendSuper_bool (this.SuperHandle, Selector.GetHandle ("setRespectAnimationFrameRate:"), value);
				}
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual bool ShouldRasterizeWhenIdle {
			[Export ("shouldRasterizeWhenIdle")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.bool_objc_msgSend (this.Handle, Selector.GetHandle ("shouldRasterizeWhenIdle"));
				} else {
					return global::ApiDefinitions.Messaging.bool_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("shouldRasterizeWhenIdle"));
				}
			}
			
			[Export ("setShouldRasterizeWhenIdle:")]
			set {
				if (IsDirectBinding) {
					global::ApiDefinitions.Messaging.void_objc_msgSend_bool (this.Handle, Selector.GetHandle ("setShouldRasterizeWhenIdle:"), value);
				} else {
					global::ApiDefinitions.Messaging.void_objc_msgSendSuper_bool (this.SuperHandle, Selector.GetHandle ("setShouldRasterizeWhenIdle:"), value);
				}
			}
		}
		
		public partial class CompatibleAnimationViewAppearance : global::UIKit.UIView.UIViewAppearance {
			protected internal CompatibleAnimationViewAppearance (IntPtr handle) : base (handle) {}
		}
		
		public static new CompatibleAnimationViewAppearance Appearance {
			get { return new CompatibleAnimationViewAppearance (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend (class_ptr, ObjCRuntime.Selector.GetHandle ("appearance"))); }
		}
		
		public static new CompatibleAnimationViewAppearance GetAppearance<T> () where T: CompatibleAnimationView {
			return new CompatibleAnimationViewAppearance (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend (Class.GetHandle (typeof (T)), ObjCRuntime.Selector.GetHandle ("appearance")));
		}
		
		public static new CompatibleAnimationViewAppearance AppearanceWhenContainedIn (params Type [] containers)
		{
			return new CompatibleAnimationViewAppearance (UIAppearance.GetAppearance (class_ptr, containers));
		}
		
		public static new CompatibleAnimationViewAppearance GetAppearance (UITraitCollection traits) {
			return new CompatibleAnimationViewAppearance (UIAppearance.GetAppearance (class_ptr, traits));
		}
		
		public static new CompatibleAnimationViewAppearance GetAppearance (UITraitCollection traits, params Type [] containers) {
			return new CompatibleAnimationViewAppearance (UIAppearance.GetAppearance (class_ptr, traits, containers));
		}
		
		public static new CompatibleAnimationViewAppearance GetAppearance<T> (UITraitCollection traits) where T: CompatibleAnimationView {
			return new CompatibleAnimationViewAppearance (UIAppearance.GetAppearance (Class.GetHandle (typeof (T)), traits));
		}
		
		public static new CompatibleAnimationViewAppearance GetAppearance<T> (UITraitCollection traits, params Type [] containers) where T: CompatibleAnimationView{
			return new CompatibleAnimationViewAppearance (UIAppearance.GetAppearance (Class.GetHandle (typeof (T)), containers));
		}
		
		
	} /* class CompatibleAnimationView */
}
