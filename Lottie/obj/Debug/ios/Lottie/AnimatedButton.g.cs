//
// Auto-generated from generator.cs, do not edit
//
// We keep references to objects, so warning 414 is expected

#pragma warning disable 414

using System;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using UIKit;
using GLKit;
using Metal;
using CoreML;
using MapKit;
using Photos;
using ModelIO;
using SceneKit;
using Contacts;
using Security;
using Messages;
using AudioUnit;
using CoreVideo;
using CoreMedia;
using QuickLook;
using CoreImage;
using SpriteKit;
using Foundation;
using CoreMotion;
using ObjCRuntime;
using AddressBook;
using MediaPlayer;
using GameplayKit;
using CoreGraphics;
using CoreLocation;
using AVFoundation;
using NewsstandKit;
using FileProvider;
using CoreAnimation;
using CoreFoundation;

namespace Lottie {
	[Register("_TtC6Lottie14AnimatedButton", true)]
	public unsafe partial class AnimatedButton : AnimatedControl {
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		static readonly IntPtr class_ptr = Class.GetHandle ("_TtC6Lottie14AnimatedButton");
		
		public override IntPtr ClassHandle { get { return class_ptr; } }
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		[Export ("init")]
		public AnimatedButton () : base (NSObjectFlag.Empty)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
			if (IsDirectBinding) {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend (this.Handle, global::ObjCRuntime.Selector.GetHandle ("init")), "init");
			} else {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSendSuper (this.SuperHandle, global::ObjCRuntime.Selector.GetHandle ("init")), "init");
			}
		}

		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[DesignatedInitializer]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		[Export ("initWithCoder:")]
		public AnimatedButton (NSCoder coder) : base (NSObjectFlag.Empty)
		{

			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
			if (IsDirectBinding) {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("initWithCoder:"), coder.Handle), "initWithCoder:");
			} else {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("initWithCoder:"), coder.Handle), "initWithCoder:");
			}
		}

		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		protected AnimatedButton (NSObjectFlag t) : base (t)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
		}

		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		protected internal AnimatedButton (IntPtr handle) : base (handle)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
		}

		[Export ("beginTrackingWithTouch:withEvent:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual bool BeginTrackingWithTouch (global::UIKit.UITouch touch, global::UIKit.UIEvent @event)
		{
			if (touch == null)
				throw new ArgumentNullException ("touch");
			if (IsDirectBinding) {
				return global::ApiDefinitions.Messaging.bool_objc_msgSend_IntPtr_IntPtr (this.Handle, Selector.GetHandle ("beginTrackingWithTouch:withEvent:"), touch.Handle, @event == null ? IntPtr.Zero : @event.Handle);
			} else {
				return global::ApiDefinitions.Messaging.bool_objc_msgSendSuper_IntPtr_IntPtr (this.SuperHandle, Selector.GetHandle ("beginTrackingWithTouch:withEvent:"), touch.Handle, @event == null ? IntPtr.Zero : @event.Handle);
			}
		}
		
		[Export ("endTrackingWithTouch:withEvent:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual void EndTrackingWithTouch (global::UIKit.UITouch touch, global::UIKit.UIEvent @event)
		{
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend_IntPtr_IntPtr (this.Handle, Selector.GetHandle ("endTrackingWithTouch:withEvent:"), touch == null ? IntPtr.Zero : touch.Handle, @event == null ? IntPtr.Zero : @event.Handle);
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper_IntPtr_IntPtr (this.SuperHandle, Selector.GetHandle ("endTrackingWithTouch:withEvent:"), touch == null ? IntPtr.Zero : touch.Handle, @event == null ? IntPtr.Zero : @event.Handle);
			}
		}
		
		public partial class AnimatedButtonAppearance : global::Lottie.AnimatedControl.AnimatedControlAppearance {
			protected internal AnimatedButtonAppearance (IntPtr handle) : base (handle) {}
		}
		
		public static new AnimatedButtonAppearance Appearance {
			get { return new AnimatedButtonAppearance (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend (class_ptr, ObjCRuntime.Selector.GetHandle ("appearance"))); }
		}
		
		public static new AnimatedButtonAppearance GetAppearance<T> () where T: AnimatedButton {
			return new AnimatedButtonAppearance (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend (Class.GetHandle (typeof (T)), ObjCRuntime.Selector.GetHandle ("appearance")));
		}
		
		public static new AnimatedButtonAppearance AppearanceWhenContainedIn (params Type [] containers)
		{
			return new AnimatedButtonAppearance (UIAppearance.GetAppearance (class_ptr, containers));
		}
		
		public static new AnimatedButtonAppearance GetAppearance (UITraitCollection traits) {
			return new AnimatedButtonAppearance (UIAppearance.GetAppearance (class_ptr, traits));
		}
		
		public static new AnimatedButtonAppearance GetAppearance (UITraitCollection traits, params Type [] containers) {
			return new AnimatedButtonAppearance (UIAppearance.GetAppearance (class_ptr, traits, containers));
		}
		
		public static new AnimatedButtonAppearance GetAppearance<T> (UITraitCollection traits) where T: AnimatedButton {
			return new AnimatedButtonAppearance (UIAppearance.GetAppearance (Class.GetHandle (typeof (T)), traits));
		}
		
		public static new AnimatedButtonAppearance GetAppearance<T> (UITraitCollection traits, params Type [] containers) where T: AnimatedButton{
			return new AnimatedButtonAppearance (UIAppearance.GetAppearance (Class.GetHandle (typeof (T)), containers));
		}
		
		
	} /* class AnimatedButton */
}
