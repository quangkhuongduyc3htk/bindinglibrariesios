//
// Auto-generated from generator.cs, do not edit
//
// We keep references to objects, so warning 414 is expected

#pragma warning disable 414

using System;
using System.Drawing;
using System.Diagnostics;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using UIKit;
using GLKit;
using Metal;
using CoreML;
using MapKit;
using Photos;
using ModelIO;
using SceneKit;
using Contacts;
using Security;
using Messages;
using AudioUnit;
using CoreVideo;
using CoreMedia;
using QuickLook;
using CoreImage;
using SpriteKit;
using Foundation;
using CoreMotion;
using ObjCRuntime;
using AddressBook;
using MediaPlayer;
using GameplayKit;
using CoreGraphics;
using CoreLocation;
using AVFoundation;
using NewsstandKit;
using FileProvider;
using CoreAnimation;
using CoreFoundation;

namespace Lottie {
	[Register("_TtC6Lottie15AnimatedControl", true)]
	public unsafe partial class AnimatedControl : global::UIKit.UIControl {
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		static readonly IntPtr class_ptr = Class.GetHandle ("_TtC6Lottie15AnimatedControl");
		
		public override IntPtr ClassHandle { get { return class_ptr; } }
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		[Export ("init")]
		public AnimatedControl () : base (NSObjectFlag.Empty)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
			if (IsDirectBinding) {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend (this.Handle, global::ObjCRuntime.Selector.GetHandle ("init")), "init");
			} else {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSendSuper (this.SuperHandle, global::ObjCRuntime.Selector.GetHandle ("init")), "init");
			}
		}

		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[DesignatedInitializer]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		[Export ("initWithCoder:")]
		public AnimatedControl (NSCoder coder) : base (NSObjectFlag.Empty)
		{

			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
			if (IsDirectBinding) {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("initWithCoder:"), coder.Handle), "initWithCoder:");
			} else {
				InitializeHandle (global::ApiDefinitions.Messaging.IntPtr_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("initWithCoder:"), coder.Handle), "initWithCoder:");
			}
		}

		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		protected AnimatedControl (NSObjectFlag t) : base (t)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
		}

		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		[EditorBrowsable (EditorBrowsableState.Advanced)]
		protected internal AnimatedControl (IntPtr handle) : base (handle)
		{
			IsDirectBinding = GetType ().Assembly == global::ApiDefinitions.Messaging.this_assembly;
		}

		[Export ("beginTrackingWithTouch:withEvent:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual bool BeginTrackingWithTouch (global::UIKit.UITouch touch, global::UIKit.UIEvent @event)
		{
			if (touch == null)
				throw new ArgumentNullException ("touch");
			if (IsDirectBinding) {
				return global::ApiDefinitions.Messaging.bool_objc_msgSend_IntPtr_IntPtr (this.Handle, Selector.GetHandle ("beginTrackingWithTouch:withEvent:"), touch.Handle, @event == null ? IntPtr.Zero : @event.Handle);
			} else {
				return global::ApiDefinitions.Messaging.bool_objc_msgSendSuper_IntPtr_IntPtr (this.SuperHandle, Selector.GetHandle ("beginTrackingWithTouch:withEvent:"), touch.Handle, @event == null ? IntPtr.Zero : @event.Handle);
			}
		}
		
		[Export ("cancelTrackingWithEvent:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual void CancelTrackingWithEvent (global::UIKit.UIEvent @event)
		{
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend_IntPtr (this.Handle, Selector.GetHandle ("cancelTrackingWithEvent:"), @event == null ? IntPtr.Zero : @event.Handle);
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper_IntPtr (this.SuperHandle, Selector.GetHandle ("cancelTrackingWithEvent:"), @event == null ? IntPtr.Zero : @event.Handle);
			}
		}
		
		[Export ("continueTrackingWithTouch:withEvent:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual bool ContinueTrackingWithTouch (global::UIKit.UITouch touch, global::UIKit.UIEvent @event)
		{
			if (touch == null)
				throw new ArgumentNullException ("touch");
			if (IsDirectBinding) {
				return global::ApiDefinitions.Messaging.bool_objc_msgSend_IntPtr_IntPtr (this.Handle, Selector.GetHandle ("continueTrackingWithTouch:withEvent:"), touch.Handle, @event == null ? IntPtr.Zero : @event.Handle);
			} else {
				return global::ApiDefinitions.Messaging.bool_objc_msgSendSuper_IntPtr_IntPtr (this.SuperHandle, Selector.GetHandle ("continueTrackingWithTouch:withEvent:"), touch.Handle, @event == null ? IntPtr.Zero : @event.Handle);
			}
		}
		
		[Export ("endTrackingWithTouch:withEvent:")]
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual void EndTrackingWithTouch (global::UIKit.UITouch touch, global::UIKit.UIEvent @event)
		{
			if (IsDirectBinding) {
				global::ApiDefinitions.Messaging.void_objc_msgSend_IntPtr_IntPtr (this.Handle, Selector.GetHandle ("endTrackingWithTouch:withEvent:"), touch == null ? IntPtr.Zero : touch.Handle, @event == null ? IntPtr.Zero : @event.Handle);
			} else {
				global::ApiDefinitions.Messaging.void_objc_msgSendSuper_IntPtr_IntPtr (this.SuperHandle, Selector.GetHandle ("endTrackingWithTouch:withEvent:"), touch == null ? IntPtr.Zero : touch.Handle, @event == null ? IntPtr.Zero : @event.Handle);
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual bool Enabled {
			[Export ("isEnabled")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.bool_objc_msgSend (this.Handle, Selector.GetHandle ("isEnabled"));
				} else {
					return global::ApiDefinitions.Messaging.bool_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("isEnabled"));
				}
			}
			
			[Export ("setEnabled:")]
			set {
				if (IsDirectBinding) {
					global::ApiDefinitions.Messaging.void_objc_msgSend_bool (this.Handle, Selector.GetHandle ("setEnabled:"), value);
				} else {
					global::ApiDefinitions.Messaging.void_objc_msgSendSuper_bool (this.SuperHandle, Selector.GetHandle ("setEnabled:"), value);
				}
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual bool Highlighted {
			[Export ("isHighlighted")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.bool_objc_msgSend (this.Handle, Selector.GetHandle ("isHighlighted"));
				} else {
					return global::ApiDefinitions.Messaging.bool_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("isHighlighted"));
				}
			}
			
			[Export ("setHighlighted:")]
			set {
				if (IsDirectBinding) {
					global::ApiDefinitions.Messaging.void_objc_msgSend_bool (this.Handle, Selector.GetHandle ("setHighlighted:"), value);
				} else {
					global::ApiDefinitions.Messaging.void_objc_msgSendSuper_bool (this.SuperHandle, Selector.GetHandle ("setHighlighted:"), value);
				}
			}
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual CGSize IntrinsicContentSize {
			[Export ("intrinsicContentSize")]
			get {
				CGSize ret;
				if (IsDirectBinding) {
					if (Runtime.Arch == Arch.DEVICE) {
						if (IntPtr.Size == 8) {
							ret = global::ApiDefinitions.Messaging.CGSize_objc_msgSend (this.Handle, Selector.GetHandle ("intrinsicContentSize"));
						} else {
							global::ApiDefinitions.Messaging.CGSize_objc_msgSend_stret (out ret, this.Handle, Selector.GetHandle ("intrinsicContentSize"));
						}
					} else if (IntPtr.Size == 8) {
						ret = global::ApiDefinitions.Messaging.CGSize_objc_msgSend (this.Handle, Selector.GetHandle ("intrinsicContentSize"));
					} else {
						ret = global::ApiDefinitions.Messaging.CGSize_objc_msgSend (this.Handle, Selector.GetHandle ("intrinsicContentSize"));
					}
				} else {
					if (Runtime.Arch == Arch.DEVICE) {
						if (IntPtr.Size == 8) {
							ret = global::ApiDefinitions.Messaging.CGSize_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("intrinsicContentSize"));
						} else {
							global::ApiDefinitions.Messaging.CGSize_objc_msgSendSuper_stret (out ret, this.SuperHandle, Selector.GetHandle ("intrinsicContentSize"));
						}
					} else if (IntPtr.Size == 8) {
						ret = global::ApiDefinitions.Messaging.CGSize_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("intrinsicContentSize"));
					} else {
						ret = global::ApiDefinitions.Messaging.CGSize_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("intrinsicContentSize"));
					}
				}
				return ret;
			}
			
		}
		
		[BindingImpl (BindingImplOptions.GeneratedCode | BindingImplOptions.Optimizable)]
		public virtual bool Selected {
			[Export ("isSelected")]
			get {
				if (IsDirectBinding) {
					return global::ApiDefinitions.Messaging.bool_objc_msgSend (this.Handle, Selector.GetHandle ("isSelected"));
				} else {
					return global::ApiDefinitions.Messaging.bool_objc_msgSendSuper (this.SuperHandle, Selector.GetHandle ("isSelected"));
				}
			}
			
			[Export ("setSelected:")]
			set {
				if (IsDirectBinding) {
					global::ApiDefinitions.Messaging.void_objc_msgSend_bool (this.Handle, Selector.GetHandle ("setSelected:"), value);
				} else {
					global::ApiDefinitions.Messaging.void_objc_msgSendSuper_bool (this.SuperHandle, Selector.GetHandle ("setSelected:"), value);
				}
			}
		}
		
		public partial class AnimatedControlAppearance : global::UIKit.UIControl.UIControlAppearance {
			protected internal AnimatedControlAppearance (IntPtr handle) : base (handle) {}
		}
		
		public static new AnimatedControlAppearance Appearance {
			get { return new AnimatedControlAppearance (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend (class_ptr, ObjCRuntime.Selector.GetHandle ("appearance"))); }
		}
		
		public static new AnimatedControlAppearance GetAppearance<T> () where T: AnimatedControl {
			return new AnimatedControlAppearance (global::ApiDefinitions.Messaging.IntPtr_objc_msgSend (Class.GetHandle (typeof (T)), ObjCRuntime.Selector.GetHandle ("appearance")));
		}
		
		public static new AnimatedControlAppearance AppearanceWhenContainedIn (params Type [] containers)
		{
			return new AnimatedControlAppearance (UIAppearance.GetAppearance (class_ptr, containers));
		}
		
		public static new AnimatedControlAppearance GetAppearance (UITraitCollection traits) {
			return new AnimatedControlAppearance (UIAppearance.GetAppearance (class_ptr, traits));
		}
		
		public static new AnimatedControlAppearance GetAppearance (UITraitCollection traits, params Type [] containers) {
			return new AnimatedControlAppearance (UIAppearance.GetAppearance (class_ptr, traits, containers));
		}
		
		public static new AnimatedControlAppearance GetAppearance<T> (UITraitCollection traits) where T: AnimatedControl {
			return new AnimatedControlAppearance (UIAppearance.GetAppearance (Class.GetHandle (typeof (T)), traits));
		}
		
		public static new AnimatedControlAppearance GetAppearance<T> (UITraitCollection traits, params Type [] containers) where T: AnimatedControl{
			return new AnimatedControlAppearance (UIAppearance.GetAppearance (Class.GetHandle (typeof (T)), containers));
		}
		
		
	} /* class AnimatedControl */
}
