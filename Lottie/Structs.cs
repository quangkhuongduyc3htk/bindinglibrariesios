﻿using System;
using CoreGraphics;
using Foundation;
using ObjCRuntime;
using UIKit;

namespace Lottie
{
    [Native]
    public enum LOTViewContentMode : ulong
    {
        ScaleToFill,
        ScaleAspectFit,
        ScaleAspectFill,
        Redraw,
        Center,
        Top,
        Bottom,
        Left,
        Right,
        TopLeft,
        TopRight,
        BottomLeft,
        BottomRight
    }
}